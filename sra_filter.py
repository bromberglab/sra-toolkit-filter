import sys
import argparse
from pathlib import Path
from os.path import commonprefix
import shutil


def runfile(*a):
    shutil.move(*a)


def parse_arguments():

    parser = argparse.ArgumentParser(
        prog="sra-toolkit filter",
        description="automatically filter the results from the sra-toolkit fasterq-dump tool",
        epilog="requires the NCBI sra-toolkit",
        formatter_class=argparse.RawDescriptionHelpFormatter
    )

    parser.add_argument('-i', '--infolder', required=True,
                        help="path to sra-toolkit results folder")
    parser.add_argument('-o', '--out1', required=True,
                        help="path to filtered fastq file")
    parser.add_argument('-O', '--out2', required=True,
                        help="path to second filtered fastq file")

    args = parser.parse_args()
    return args


def main():
    args = parse_arguments()
    sra_folder = Path(args.infolder)
    out1 = Path(args.out1)
    out2 = Path(args.out2)
    sra_acc = commonprefix(
        [_.stem for _ in sra_folder.glob("*.fastq")]).replace("_", "")

    target_file = sra_folder / f'{sra_acc}.fastq'
    target_file_R1 = sra_folder / f'{sra_acc}_1.fastq'
    target_file_R2 = sra_folder / f'{sra_acc}_2.fastq'
    target_file_R1_2 = sra_folder / f'{sra_acc}_2.fastq'
    target_file_R2_4 = sra_folder / f'{sra_acc}_4.fastq'

    if target_file.exists():
        print("Found single fastq")
        runfile(target_file, out1)
    elif target_file_R1.exists() and target_file_R2.exists():
        print("Found R1/R2 (_1,_2) fastqs")
        runfile(target_file_R1, out1)
        runfile(target_file_R2, out2)
    elif target_file_R1_2.exists() and target_file_R2_4.exists():
        print("Found R1/R2 (_2,_4) fastqs")
        runfile(target_file_R1_2, out1)
        runfile(target_file_R2_4, out2)
    elif target_file_R1.exists():
        print("Found single R1 (_1) fastq")
        runfile(target_file_R1, out1)
    elif target_file_R1_2.exists():
        print("Found single R1 (_2) fastq")
        runfile(target_file_R1_2, out1)
    else:
        exit(f"ERROR - Could not filter for SRA: {sra_acc}")


if __name__ == "__main__":
    main()
